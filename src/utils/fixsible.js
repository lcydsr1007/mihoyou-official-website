let html = document.documentElement;
window.addEventListener("resize", fixsible);
function fixsible() {
  let windowSize = window.innerWidth;
  document.documentElement.style.fontSize =
    windowSize < 1200 ? 1200 / 24 + "px" : windowSize / 24 + "px";
}
fixsible();
